import Dependencies._

lazy val root =
  (project in file("."))
    .settings(
      inThisBuild(List(
        organization := "co.technius",
        scalaVersion := "2.12.2",
        version      := "0.1.0-SNAPSHOT",
        scalacOptions ++= Seq(
          "-deprecation",
          "-unchecked",
          "-feature",
          "-Xlint:-unused,_",
          "-Xfuture",
          "-Xfatal-warnings",
          "-Ypartial-unification",
          "-Yno-adapted-args",
          "-explaintypes"
        )
      ))
    )
    .aggregate(core)


lazy val core =
  (project in file("core"))
    .settings(
      name := "fintag-core",
      libraryDependencies ++= Seq(
        tagging,
        scalaTest % Test,
        scalaXml % Test
      ),
      addCompilerPlugin(kindProjector)
    )
