import org.scalatest._

import co.technius.fintags._
import co.technius.fintags.{ Attrs => A, Tags => T }
import scala.xml

class FintagsSpec extends FlatSpec with Matchers {
  val basicTag =
    T.div(
      T.p(Seq(A.id := "hello"), "Hello world!"),
      T.p(
        "This is fintags!",
        T.br.apply,
        "Generate some HTML",
        "at high speeds"
      ),
      T.div(
        T.p("Why fintags?"),
        T.ul(
          T.li("Simple to use"),
          T.li("Tags are type safe, including parent and child tags!"),
          T.li("HTML with invalid content is rarely, if ever, generated")
        )
      )
    )

  "Tag definitions" should "not compile tags with unallowed content" in {
    """T.ol(T.div())""" shouldNot typeCheck
    """T.ul(T.div())""" shouldNot typeCheck
  }

  it should "restrict metadata content in head" in {
    """
    T.head(
      T.title("foo"),
      T.base(Seq("href" -> "http://www.example.com")),
      T.link(Seq("rel" -> "stylesheet", "type" -> "text/css", "href" -> "/styles.css")),
      T.meta(Seq("charset" -> "UTF-8")),
      T.style(".foo { color: blue; }")
    )
    """.stripMargin should compile

    "T.head(div())" shouldNot typeCheck
  }

  it should "properly restrict text content" in {
    """T.script("console.log('Hello world');")""" should compile
    """T.script(T.div())""" shouldNot typeCheck
  }

  "Text tags" should "render" in {
    val output = basicTag.render(TextTags)
    val tag = xml.XML.loadString(output)
    tag.child.length should be (3)
    (tag.child(0) \@ "id") should be ("hello")
    tag.child(2).length should be (1)
  }

  it should "escape quotes in attributes" in {
    val testTag = T.div(Seq("data-foo" -> "\"><script>console.log(\"XSS attack\")</script><div data-bar=\""))
    val tag = xml.XML.loadString(testTag.render(TextTags))
    (tag \@ "data-foo") should not be empty
    tag.child.length should be (0)
  }

  it should "escape text content" in {
    val testTag = T.div("""<script>console.log("XSS attack")</script><div>Pwned</div>""")
    val tag = xml.XML.loadString(testTag.render(TextTags))
    tag.child.length should be (1) // text node
    tag.child(0).label should not be ("script")
  }
}
