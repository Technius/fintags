package co.technius.fintags

/**
  * Describes categories of content permitted by each tag.
  */
sealed trait PermittedContent
object PermittedContent {
  /**
    * Permits all tags to be children.
    */
  type All = PermittedContent

  /**
    * Permits only text nodes to be children.
    */
  sealed trait Text extends PermittedContent

  /**
    * Permits document metadata to be children.
    */
  sealed trait Metadata extends PermittedContent

  /**
    * Permits only list items to be children.
    */
  sealed trait ListItem extends PermittedContent
}
