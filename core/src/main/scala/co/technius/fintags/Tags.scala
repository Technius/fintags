package co.technius.fintags

import scala.language.implicitConversions
import com.softwaremill.tagging._

/**
  * Defines tag builder objects for constructing HTML documents. They are
  * organized according to the MDN HTML element reference.
  */
trait Tags {
  import PermittedContent._

  object html extends Tags.TagBuilder[All, All]("html")

  // Metadata
  object base extends Tags.VoidTagBuilder[Metadata]("base")
  object head extends Tags.TagBuilder[All, Metadata]("head")
  object link extends Tags.VoidTagBuilder[Metadata]("link")
  object meta extends Tags.VoidTagBuilder[Metadata]("meta")
  object style extends Tags.TagBuilder[Metadata, Text]("style")
  object title extends Tags.TagBuilder[Metadata, Text]("title")

  // Content sectioning
  object address extends Tags.TagBuilder[All, All]("address")
  object article extends Tags.TagBuilder[All, All]("article")
  object aside extends Tags.TagBuilder[All, All]("aside")
  object footer extends Tags.TagBuilder[All, All]("footer")
  object h1 extends Tags.TagBuilder[All, All]("h1")
  object h2 extends Tags.TagBuilder[All, All]("h2")
  object h3 extends Tags.TagBuilder[All, All]("h3")
  object h4 extends Tags.TagBuilder[All, All]("h4")
  object h5 extends Tags.TagBuilder[All, All]("h5")
  object h6 extends Tags.TagBuilder[All, All]("h6")
  object header extends Tags.TagBuilder[All, All]("header")
  object hgroup extends Tags.TagBuilder[All, All]("hgroup")
  object nav extends Tags.TagBuilder[All, All]("nav")
  object section extends Tags.TagBuilder[All, All]("section")

  // Text content
  object blockquote extends Tags.TagBuilder[All, All]("blockquote")
  object dd extends Tags.TagBuilder[All, All]("dd")
  object div extends Tags.TagBuilder[All, All]("div")
  object dl extends Tags.TagBuilder[All, All]("dl")
  object dt extends Tags.TagBuilder[All, All]("dt")
  object figcaption extends Tags.TagBuilder[All, All]("figcaption")
  object figure extends Tags.TagBuilder[All, All]("figure")
  object hr extends Tags.TagBuilder[All, All]("hr")
  object li extends Tags.TagBuilder[ListItem, All]("li")
  object main extends Tags.TagBuilder[All, All]("main")
  object ol extends Tags.TagBuilder[All, ListItem]("ol")
  object p extends Tags.TagBuilder[All, All]("p")
  object pre extends Tags.TagBuilder[All, All]("pre")
  object ul extends Tags.TagBuilder[All, ListItem]("ul")

  // Inline text semantics
  object a extends Tags.TagBuilder[All, All]("a")
  object abbr extends Tags.TagBuilder[All, All]("abbr")
  object b extends Tags.TagBuilder[All, All]("b")
  object bdi extends Tags.TagBuilder[All, All]("bdi")
  object bdo extends Tags.TagBuilder[All, All]("bdo")
  object br extends Tags.VoidTagBuilder[All]("br")
  object cite extends Tags.TagBuilder[All, All]("cite")
  object code extends Tags.TagBuilder[All, All]("code")
  object data extends Tags.TagBuilder[All, All]("data")
  object dfn extends Tags.TagBuilder[All, All]("dfn")
  object em extends Tags.TagBuilder[All, All]("em")
  object i extends Tags.TagBuilder[All, All]("i")
  object kbd extends Tags.TagBuilder[All, All]("kbd")
  object mark extends Tags.TagBuilder[All, All]("mark")
  object q extends Tags.TagBuilder[All, All]("q")
  object rp extends Tags.TagBuilder[All, All]("rp")
  object rt extends Tags.TagBuilder[All, All]("rt")
  object rtc extends Tags.TagBuilder[All, All]("rtc")
  object ruby extends Tags.TagBuilder[All, All]("ruby")
  object s extends Tags.TagBuilder[All, All]("s")
  object samp extends Tags.TagBuilder[All, All]("samp")
  object small extends Tags.TagBuilder[All, All]("small")
  object span extends Tags.TagBuilder[All, All]("span")
  object strong extends Tags.TagBuilder[All, All]("strong")
  object sub extends Tags.TagBuilder[All, All]("sub")
  object time extends Tags.TagBuilder[All, All]("time")
  object u extends Tags.TagBuilder[All, All]("u")
  object `var` extends Tags.TagBuilder[All, All]("var")
  object wbr extends Tags.TagBuilder[All, All]("wbr")

  // Image and multimedia
  object area extends Tags.TagBuilder[All, All]("area")
  object audio extends Tags.TagBuilder[All, All]("audio")
  object img extends Tags.VoidTagBuilder[All]("img")
  object map extends Tags.TagBuilder[All, All]("map")
  object track extends Tags.VoidTagBuilder[All]("track")
  object video extends Tags.TagBuilder[All, All]("video")

  // Embedded content
  object embed extends Tags.VoidTagBuilder[All]("embed")
  object `object` extends Tags.VoidTagBuilder[All]("object")
  object param extends Tags.VoidTagBuilder[All]("param")
  object source extends Tags.VoidTagBuilder[All]("source")

  // Scripting
  object canvas extends Tags.TagBuilder[All, All]("canvas")
  object noscript extends Tags.TagBuilder[All, Metadata]("noscript")
  object script extends Tags.TagBuilder[All, Text]("script")

  // Demarcating edits
  object del extends Tags.TagBuilder[All, All]("del")
  object ins extends Tags.TagBuilder[All, All]("ins")

  // Table content
  object caption extends Tags.TagBuilder[All, All]("caption")
  object col extends Tags.TagBuilder[All, All]("col")
  object colgroup extends Tags.TagBuilder[All, All]("colgroup")
  object table extends Tags.TagBuilder[All, All]("table")
  object tbody extends Tags.TagBuilder[All, All]("tbody")
  object td extends Tags.TagBuilder[All, All]("td")
  object tfoot extends Tags.TagBuilder[All, All]("tfoot")
  object th extends Tags.TagBuilder[All, All]("th")
  object thead extends Tags.TagBuilder[All, All]("thead")
  object tr extends Tags.TagBuilder[All, All]("tr")

  // Forms
  object button extends Tags.TagBuilder[All, All]("button")
  object datalist extends Tags.TagBuilder[All, All]("datalist")
  object fieldset extends Tags.TagBuilder[All, All]("fieldset")
  object form extends Tags.TagBuilder[All, All]("form")
  object input extends Tags.VoidTagBuilder[All]("input")
  object label extends Tags.TagBuilder[All, All]("label")
  object legend extends Tags.TagBuilder[All, All]("legend")
  object meter extends Tags.TagBuilder[All, All]("meter")
  object optgroup extends Tags.TagBuilder[All, All]("optgroup")
  object option extends Tags.TagBuilder[All, All]("option")
  object output extends Tags.TagBuilder[All, All]("output")
  object progress extends Tags.TagBuilder[All, All]("progress")
  object select extends Tags.TagBuilder[All, All]("select")
  object textarea extends Tags.TagBuilder[All, All]("textarea")

  // Interactive elements
  object details extends Tags.TagBuilder[All, All]("details")
  object dialog extends Tags.TagBuilder[All, All]("dialog")
  object menu extends Tags.TagBuilder[All, All]("menu")
  object menuitem extends Tags.TagBuilder[All, All]("menuitem")
  object summary extends Tags.TagBuilder[All, All]("summary")
}

object Tags extends Tags {
  /**
    * Extend this class to create a custom "typed tag".
    * @tparam Parent The type of content that the parent of this tag should have.
    * @tparam Child The type of content that this tag may contain.
    */
  abstract class TagBuilder[Parent <: PermittedContent, Child <: PermittedContent](tagName: String) {

    /**
      * Constructs a TagTree element with no attributes and the given children.
      */
    def apply(children: TagTree @@ Child*): TagTree @@ Parent =
      TagTree.Element(tagName, Seq.empty, children).taggedWith[Parent]

    /**
      * Constructs a TagTree element with the given attributes and children.
      */
    def apply(attrs: Seq[(String, String)], children: TagTree @@ Child*): TagTree @@ Parent =
      TagTree.Element(tagName, attrs, children).taggedWith[Parent]
  }

  /**
    * Same as TagBuilder, except for void (empty) tags.
    */
  abstract class VoidTagBuilder[Parent <: PermittedContent](tagName: String) {
    /**
      * Constructs a TagTree void element with no attributes.
      */
    def apply(): TagTree @@ Parent =
      TagTree.VoidElement(tagName, Seq.empty).taggedWith[Parent]

    /**
      * Constructs a TagTree void element with the given attributes.
      */
    def apply(attrs: Seq[(String, String)]): TagTree @@ Parent =
      TagTree.VoidElement(tagName, attrs).taggedWith[Parent]
  }

}
