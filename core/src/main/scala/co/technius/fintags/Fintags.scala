package co.technius.fintags

import com.softwaremill.tagging._
import scala.language.implicitConversions

/**
  * The algebra that describes how to construct tags.
  * @tparam Out The target type that the tag should be constructed into.
  */
abstract class TagAlg[Out] {
  /**
    * Renders an entire tag tree into the specified output format.
    */
  def render(tree: TagTree): Out = tree match {
    case TagTree.Text(t) => textNode(t)
    case TagTree.VoidElement(tagName, attrs) => createVoidTag(tagName, attrs)
    case TagTree.Element(tagName, attrs, tagChildren) =>
      val children = tagChildren.map(render(_))
      createTag(tagName, attrs, children)
  }

  /**
    * Renders a _single_ non-void tag, given the children it should render.
    * @param tagName The tag name
    * @param children The children of this tag in the output format.
    * @param attrs The attributes belonging to this tag
    */
  def createTag(tagName: String, attrs: TagTree.AttrList, children: Seq[Out]): Out

  /**
    * Renders a void tag,
    * @param tagName The tag name
    * @param attrs The attributes belonging to this tag
    */
  def createVoidTag(tagName: String, attrs: TagTree.AttrList): Out

  /**
    * Creates a text node. Note that the parameter is an unescaped string, so
    * the text must be escaped in this method if the algebra is outputting raw
    * HTML.
    */
  def textNode(text: String): Out
}

sealed abstract class TagTree {
  def render[T](implicit alg: TagAlg[T]): T = alg.render(this)
}

object TagTree {
  type AttrList = Seq[(String, String)]
  case class Text(value: String) extends TagTree

  case class Element(
    tagName: String,
    attrs: AttrList,
    children: Seq[TagTree]) extends TagTree

  case class VoidElement(
    tagName: String,
    attrs: AttrList) extends TagTree

  implicit def liftText(value: String): TagTree.Text @@ PermittedContent.Text =
    TagTree.Text(value).taggedWith[PermittedContent.Text]
}

/**
  * An implementation of TagAlg for outputting HTML as a String.
  */
object TextTags extends TagAlg[String] {
  private def buildAttrString(attrs: Seq[(String, Any)]): String =
    attrs.view
      .map { pair =>
        val name = pair._1
        val value = Escaping.escapeAttrValue(pair._2.toString)
        if (value.isEmpty) name else name + "=\"" + value + "\""
      }
      .mkString(" ")

  def createTag(tagName: String, attrs: TagTree.AttrList, children: Seq[String]): String = {
    val attrStr = buildAttrString(attrs)
    s"""<${tagName}${if (attrStr.isEmpty) "" else " " + attrStr}>${children.mkString}</$tagName>"""
  }

  def createVoidTag(tagName: String, attrs: TagTree.AttrList): String = {
    val attrStr = buildAttrString(attrs)
    s"""<${tagName} ${if (attrStr.isEmpty) "" else attrStr + " "}/>"""
  }

  def textNode(text: String): String = Escaping.escapeTextContent(text)
}

/**
  * Utilities for escaping HTML text and attributes.
  */
object Escaping {
  def escapeTextContent(value: String): String =
    value
      .replaceAll("&", "&amp;")
      .replaceAll("<", "&lt;")
      .replaceAll(">", "&gt;")

  def escapeAttrValue(value: String): String =
    escapeTextContent(value).replaceAll("\"", "&quot;")
}
