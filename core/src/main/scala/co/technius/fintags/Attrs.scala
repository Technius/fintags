package co.technius.fintags

trait Attrs {
  val id = Attribute[String]("id")(identity)
  val `class` = Attribute[Seq[String]]("class")(_.mkString(" "))
  val style = Attribute[Seq[(String, String)]]("style") { s =>
    s.map { case (key, value) => s"$key:$value" }.mkString(";")
  }

  def attr(attrName: String, value: String): (String, String) = attrName -> value
}
object Attrs extends Attrs

case class Attribute[Value](name: String)(serialize: Value => String) {
  def :=(value: Value): (String, String) =
    name -> serialize(value)
}
