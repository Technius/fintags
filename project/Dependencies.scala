import sbt._

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.1"
  lazy val scalaXml = "org.scala-lang.modules" %% "scala-xml" % "1.0.6"
  lazy val tagging = "com.softwaremill.common" %% "tagging" % "2.1.0"
  lazy val kindProjector = "org.spire-math" %% "kind-projector" % "0.9.4"
}
