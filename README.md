# Fintags

This is an attempt at a simple, fully type-safe, high-performance HTML
construction library, inspired by scalatags and type-safe libraries such as
pureconfig.
